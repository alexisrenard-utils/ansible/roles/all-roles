# Alexis Renard's roles repositories

## Usage

* Clone (or fork) the project and its submodule(s)
```bash
git clone --recurse-submodules <this repo>
```
* Initiate the submodule(s) recursively - the right way
```bash
git submodule update --init --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```
* Use the roles in your project, adding them as git submodules in your `/roles` directory (see the [Appendix](#usage))

## Appendix : Working with git submodules

* Add a submodule
```bash
git submodule add <repo_url>
```

* Update the submodules
```bash
git submodule update --init --recursive # the init option is not harmful to already initialized submodules
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```

* Remove a gitsubmodule in a proper way ([check out the mess over there](https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule/36593218#36593218))
```bash
# Remove the submodule entry from .git/config
git submodule deinit -f path/to/submodule

# Remove the submodule directory from the superproject's .git/modules directory
rm -rf .git/modules/submodule_name

# Remove the entry in .gitmodules and remove the submodule directory located at path/to/submodule
git rm -f path/to/submodule
```
*git version 2.17.1*

## Author Information
This project was created by: [alexis_renard](https://renardalexis.com)

👉 https://fr.linkedin.com/in/renardalexis
